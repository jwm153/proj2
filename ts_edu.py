
import threading
import sys

import socket


def server():
    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: Server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    tsEduListenPort = int(sys.argv[1])
    # tsEduListenPort = 8080
    TSEDUDNS = open("PROJ2-DNSTSedu.txt", "r")
    DNSList = []

    line = TSEDUDNS.readline().strip()
    while line != "":
        DNSList.append(line)
        line = TSEDUDNS.readline().strip()

    server_binding = ('', tsEduListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = socket.gethostname()
    print("[S]: Server host name is {}".format(host))
    localhost_ip = (socket.gethostbyname(host))
    print("[S]: Server IP address is {}".format(localhost_ip))
    csockid, addr = ss.accept()
    print ("[S]: Got a connection request from a client at {}".format(addr))

    data = csockid.recv(100).decode("utf-8").strip()
    input = []
    x = 0
    while data != "EOF":
        input.append(data)

        y = 0
        match = "no"
        while y < DNSList.__len__():
            url = DNSList[y].split()
            if input[x].lower() == url[0].lower():
                match = DNSList[y]
            elif match == "no":
                match = input[x] + " - Error:HOST NOT FOUND"
            y += 1
        csockid.send(match)
        data = csockid.recv(100).decode("utf-8").strip()
        x += 1


    # Close the server socket
    ss.close()
    exit()


t1 = threading.Thread(name='server', target=server)
t1.start()

import threading
import time
import sys

import socket


def client():
    try:
        rs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[C]: RS socket created")
    except socket.error as err:
        print('socket open error: {} \n'.format(err))
        exit()

    # Define the port on which you want to connect to the server
    rsHostName = sys.argv[1]
    rsListenPort = int(sys.argv[2])

    # used for testing in IDE (Pycharm)
    # rsHostName = "localhost"
    # rsListenPort = 50007

    localhost_addr = socket.gethostbyname(rsHostName)

    server_binding = (localhost_addr, rsListenPort)
    rs.connect(server_binding)

    output = open("RESOLVED.txt", "w")

    ClientHNS = open("PROJ2-HNS.txt", "r")
    HNS = []

    line = ClientHNS.readline().strip()
    HNS.append(line)
    while line != "":
        line = ClientHNS.readline().strip()
        HNS.append(line)

    print("\n")

    x = 0
    while x < HNS.__len__():
        if HNS[x] == "":
            rs.sendall("EOF")
        else:
            rs.sendall(HNS[x].encode("utf-8"))
        time.sleep(1)
        data_from_server = rs.recv(100)
        print(data_from_server)
        output.write(data_from_server + "\n")
        x += 1

    # close the client socket
    rs.close()

    exit()


t2 = threading.Thread(name='client', target=client)
t2.start()

0.
	Jessi Medina: jwm153
	Hardil Vadi: hnv6

1. 
	Our recursive client begins by reading in the HNS file and parsing it, placing each of the different URL requests into separate list items. It then sends them one by one to the RS server which checks the requested URL against its list[] of domains compiled from the RSDNS file. The RS server reads in the DNS file on startup and makes a list[] of the URLs in it, while separating out the locations of ts_edu and ts_com and storing that information elsewhere. If the RS list of domains contains the requested URL, the RS returns that item in its list to the client. If the RS list does not contain the URL, it checks if the URL contains ".com" or ".edu" and sends it to the appropriate server, ts_com or ts_edu respectively. If the RS does not have the URL, AND it does not contain ".com" or ".edu", then it simply sends "Error: HOST NOT FOUND" back to the client.
	Once either the ts_com or ts_edu servers get a request, they check their list[] that they made from their DNS file. If they have a match they send that back to RS who sends it to Client. If they do not have a match, they send "Error: HOST NOT FOUND" to RS who sends it to client.
The Client then writes whatever response it gets from RS to the output file.

2. 
	We have only come across one issue with the code while testing the final product. Across all wifi networks that I have tested on, the code has worked perfectly fine, and it also works fine on the iLab machines. But when I attempted to test my code on the wifi network at my place of employment, the servers would crash with the error: [Errno 8] nodename nor servname provided, or not known. I'm not sure why the wifi at my job was causing this error, but everywhere else I have tested my code it has worked perfectly fine. (This problem persists from project 1, I'm assuming it is a problem with the wifi, not the program)
	Other than the above mentioned hiccup, there are no known issues with our code. 

3.
	For a short time in the iteration of our code, we had the RS sending and receiving way more messages and responses to and from the TS servers, which caused our program to take VERY long to complete, although it would finish eventually. This was resolved.
	Another problem I ran across was when testing the programs on the iLab computers. I frustratingly tried to figure out why the programs would not work when some servers were hosted on the iLab and some on my personal computer. Eventually I realized that you needed to change the IP address of the TS servers in the DNS for the RS server. I'll consider it a learning experience. We did not encounter many other issues when developing the code since a lot of the headaches that we had when creating Project 1 had already been figured out and a lot of code was able to be re-used.

4. 
	We've learned how deceptively simple it is to make a server communicate to other servers. Initially we anticipated it being at least a little difficult/different to make the RS server communicate with the TS servers instead of just communicating with the client. It turns out it is actually very simple to do. I have also added onto my knowledge of Python, which is quickly becoming my favorite coding language.
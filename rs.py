
import threading
import sys
import time

import socket


def server():
    try:
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: Client server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()
    try:
        tse = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: ts_edu server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()
    try:
        tsc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("[S]: ts_com server socket created")
    except socket.error as err:
        print('socket open error: {}\n'.format(err))
        exit()

    rsListenPort = int(sys.argv[1])
    tsEduListenPort = int(sys.argv[2])
    tsComListenPort = int(sys.argv[3])

    # Values used for testing in IDE (pycharm)
    # rsListenPort = 50007
    # tsEduListenPort = 8080
    # tsComListenPort = 666

    RSDNS = open("PROJ2-DNSRS.txt", "r")
    DNSList = []

    line = RSDNS.readline().strip()
    while line != "":
        if " NS" in line:
            if ".com" in line:
                ts_com_info = line.split()
            if ".edu" in line:
                ts_edu_info = line.split()
        else:
            DNSList.append(line)
        line = RSDNS.readline().strip()

    server_binding = ('', rsListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = socket.gethostname()
    print("[S]: Server host name is {}".format(host))
    localhost_ip = (socket.gethostbyname(host))
    print("[S]: Server IP address is {}".format(localhost_ip))
    csockid, addr = ss.accept()
    print ("[S]: Got a connection request from a client at {}".format(addr))

    name = socket.gethostbyaddr(ts_edu_info[1])[0]
    localhost_addr = socket.gethostbyname(name)
    server_binding_edu = (localhost_addr, tsEduListenPort)
    tse.connect(server_binding_edu)

    name = socket.gethostbyaddr(ts_com_info[1])[0]
    localhost_addr = socket.gethostbyname(name)
    server_binding_com = (localhost_addr, tsComListenPort)
    tsc.connect(server_binding_com)

    data = csockid.recv(100).decode("utf-8").strip()
    input = []

    x = 0
    while data != "EOF":
        input.append(data)

        y = 0
        match = "no"
        while y < DNSList.__len__():
            url = DNSList[y].split()
            if input[x].lower() == url[0].lower():
                match = DNSList[y]
            elif match == "no" and ".edu" in input[x].lower():
                tse.sendall(data)
                time.sleep(1)
                data_from_edu = tse.recv(100)
                match = data_from_edu
            elif match == "no" and ".com" in input[x].lower():
                tsc.sendall(data)
                time.sleep(1)
                data_from_com = tsc.recv(100)
                match = data_from_com
            elif ".com" not in input[x].lower() and ".edu" not in input[x].lower():
                match = input[x] + " - Error:HOST NOT FOUND"
            y += 1
        csockid.send(match)

        data = csockid.recv(100).decode("utf-8").strip()
        x += 1
    tse.sendall("EOF")
    tsc.sendall("EOF")

    tse.close()
    tsc.close()
    ss.close()
    exit()


t1 = threading.Thread(name='server', target=server)
t1.start()
